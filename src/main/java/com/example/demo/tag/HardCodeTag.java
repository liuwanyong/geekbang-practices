package com.example.demo.tag;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;
import java.io.PrintWriter;

public class HardCodeTag extends SimpleTagSupport {
    @Override
    public void doTag() throws JspException, IOException {
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletResponse response = attributes.getResponse();
        response.setContentType("UTF-8");
        response.setHeader("Cache-Control", "no-cache");
        response.setHeader("Pragma", "no-cache");
        response.setDateHeader("Expires", -1);
        PrintWriter printWriter = response.getWriter();
        printWriter.println("<html>");
        printWriter.println("<title>");
        printWriter.println(title==null?"undefined":title);
        printWriter.println("</title>");
        printWriter.println("<body>");
        printWriter.println(bodyContent==null?"":bodyContent);
        printWriter.println("</body>");
        printWriter.println("</html>");

    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    String title;

    public String getBodyContent() {
        return bodyContent;
    }

    public void setBodyContent(String bodyContent) {
        this.bodyContent = bodyContent;
    }

    String bodyContent;

}
